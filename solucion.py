import json
import logging
import os
import sys
import time

class GetQuestionsAnswerSend:
    def __init__(self, fetch_chunk_size=1):
        self.fetch_chunk_size = fetch_chunk_size
        self.count_total = 0
        self.send_retried = 0
        self.log = None

    def set_logging(self):
        self.log = logging.getLogger(__name__)
        self.log.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(name)s %(asctime)s %(levelname)s:: %(message)s")
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        self.log.addHandler(handler)

    def do_work_loop(self, list_asc, sum, starting_from=0):
        # if not self.log:
        #     self.set_logging()

        self.count_total = self.fetch_chunk_size
        sent = starting_from
        
        while sent < self.count_total:
            try:
                sent, matching, response_code = self._get_send(list_asc, sum,sent)
                print("______",sent, matching, response_code)
            except Exception as e:
                print(str(e))

        return sent, matching, response_code

    def _get_send(self,list_asc, sum, _get_from=0):
        start = time.time()
        _from = _get_from 
        _to = _from + self.fetch_chunk_size
        matching = []
        flag=False
        self.log.debug(f"Ejecutando Script desde {_from} - hasta {_to}")
        for list_num in list_asc:
            if flag==True:break
            for sum_num in list_asc:
                if list_num+sum_num == sum:
                    matching.append((list_num,sum_num))
                    flag=True
        # l = [matching.append((list_num,sum_num)) for list_num in list_asc for sum_num in list_asc if list_num+sum_num == sum break]
        
        self.log.debug("tiempo final: %s", time.time() - start)

        self.log.debug(
            f"Script finalizado con Exito desde {_from} - hasta {_to}"
        )

        return _to, matching, 200


#Parametros: 
# GetQuestionsAnswerSend(10) Opcional ingresas la cantidad de vueltas o inicio del bucle
# es un valor tambien personalizable por si requerimos algun corte recurrente para cantidades muy grandes de miles por ejemplo de manera recurrente o para un api.
run_script = GetQuestionsAnswerSend()
run_script.set_logging()
# Parametros de la funcion: 
# do_work_loop(lista_ascendente,suma_predeterminada,inicio_de_vueltas_requeridas)
# lista_ascendente: Lista de valores a evaluar
# suma_predeterminada: Suma requerida para encontrar el par
# inicio_de_vueltas_requeridas: Opcional como plus para personalizar un poco mas la funcion
x = range(10000)
get_run_script = run_script.do_work_loop(list(x),7000,0) 

