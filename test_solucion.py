from solucion import GetQuestionsAnswerSend

def test_get_send():
    listado = [2,3,6,7]
    _class_main_test = GetQuestionsAnswerSend(1)
    sent, matching, response_code = _class_main_test.do_work_loop(listado,9,0)

    assert response_code == (1, [3,6], 200)
